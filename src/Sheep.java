import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.Display;

public class Sheep extends MIDlet {

    public SheepCanvas canvas;

    public Sheep() {

        canvas = new SheepCanvas();
        canvas.setMIDlet(this);
        canvas.start();

    }

    public void startApp() {

        Display.getDisplay(this).setCurrent(canvas);

    }

    public void pauseApp() {

    }

    public void destroyApp(boolean conditional) {
    }

}


