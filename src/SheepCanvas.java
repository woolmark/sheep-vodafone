import java.util.Random;
import java.io.*;
import javax.microedition.midlet.MIDlet;
import javax.microedition.io.*;
import javax.microedition.lcdui.*;
import javax.microedition.rms.*;

/**
 * Sheep Canvas.
 *
 * @author takkie
 */
public class SheepCanvas extends Canvas implements Runnable, CommandListener {

    /**
     * the "0" button.
     */
    public static final int KEY_0 = 0;

    /**
     * the "1" button.
     */
    public static final int KEY_1 = 1;

    /**
     * the "2" button.
     */
    public static final int KEY_2 = 2;

    /**
     * the "3" button.
     */
    public static final int KEY_3 = 3;

    /**
     * the "4" button.
     */
    public static final int KEY_4 = 4;

    /**
     * the "5" button.
     */
    public static final int KEY_5 = 5;

    /**
     * the "6" button.
     */
    public static final int KEY_6 = 6;

    /**
     * the "7" button.
     */
    public static final int KEY_7 = 7;

    /**
     * the "8" button.
     */
    public static final int KEY_8 = 8;

    /**
     * the "9" button.
     */
    public static final int KEY_9 = 9;

    /**
     * the "*" button.
     */
    public static final int KEY_00 = 10;

    /**
     * the "#" button.
     */
    public static final int KEY_01 = 11;

    /**
     * the "UP" button.
     */
    public static final int KEY_UP = 13;

    /**
     * the "RIGHT" button.
     */
    public static final int KEY_RIGHT = 14;

    /**
     * the "DOWN" button.
     */
    public static final int KEY_DOWN = 15;

    /**
     * the "LEFT" button.
     */
    public static final int KEY_LEFT = 16;

    /**
     * the "SELECT" button.
     */
    public static final int KEY_SELECT = 17;

    /**
     *  the button.
     */
    public static final int KEY_SOFT1 = 18;

    /**
     *  the button.
     */
    public static final int KEY_SOFT2= 19;

    private static Sheep midlet;

    /*
     *
     */
    private static int i, j, k;

    /*
     *
     */
    private static int l, m, n;

    private Command cmdZZ = null;

    /**
     * Thread.
     */
    private Thread runner;

    /**
     * create random number.
     */
    private static Random rand;

    /**
     * the system font type.
     */
    private static Font font;

    /**
     * the application state.
     */
    private static int state;

    /**
     * the sheep number.
     */
    private static int sheep_number;

    /**
     * the sheep image.
     */
    private Image sheep_image[];

    /**
     * the frame image.
     */
    private Image background;

    /**
     * the sheep position.
     */
    private static int sheep_pos[][] = new int[100][3];

    /**
     * key flag.
     */
    private static int key_flag;

    /**
     * do nothing.
     */
    public SheepCanvas() {

    }

    public static void setMIDlet(Sheep mid) {

        midlet = mid;

    }

    /**
     * start application.
     */
    public void start() {
        runner = new Thread(this);
        runner.start();
    }

    /**
     * run method.
     */
    public void run() {

        /* create object */
        state = 1;
        rand = new Random();
        font = Font.getDefaultFont();
        cmdZZ = new Command("ZZ", Command.SCREEN, 1);

        setCommandListener(this);

        sheep_image = new Image[2];

        try {
            sheep_image[0] = Image.createImage("/sheep00.png");
            sheep_image[1] = Image.createImage("/sheep01.png");

            background = Image.createImage("/back.png");
        } catch (Exception e) {
            System.out.println(e);
        }

        // sheep_number = readInt("sheep_number");   
        sheep_number = 0;

        /* set the sheep position */
        for(i = 0; i < sheep_pos.length; i++) {
            sheep_pos[i][0] = 0;
            sheep_pos[i][1] = -1;
            sheep_pos[i][2] = 0;
        }

        sheep_pos[0][0] = (getWidth() - getWidth() % 10) + 10;
        sheep_pos[0][1] = (getHeight() - 40) + rand.nextInt() % 30;
        sheep_pos[0][2] = 0;

        addCommand(new Command("", Command.SCREEN, 0));
        addCommand(cmdZZ);

        state = 5;

        while(true) {

            if(state == -1) {
                break;
            }

            calc();

            repaint();

            try {
                serviceRepaints();
                Thread.sleep(60);
            } catch(Exception exception) {
            }

        }

    }

    /**
     * calculation method.
     */
    private void calc() {

        if(state == 5) {

            /* run the sheep */
            if(isKeyPressed(KEY_SELECT)) {

                /* add a new sheep */
                for(i = 1; i < sheep_pos.length; i++) {

                    if(sheep_pos[i][1] == -1) {

                        sheep_pos[i][0] = getWidth() + 20;
                        sheep_pos[i][1] = (getHeight() - 40) + rand.nextInt() % 30;
                        sheep_pos[i][2] = 0;

                        break;

                    }

                }

            }

            /* run the sheep */
            for(i = 0; i < sheep_pos.length; i++) {
                if(sheep_pos[i][1] >= 0) {

                    /* remove a frameouted sheep */
                    if((sheep_pos[i][0] -= 5) < -20) {
                        if(i == 0) {
                            sheep_pos[0][0] = getWidth() + 20;
                            sheep_pos[0][1] = (getHeight() - 40) + rand.nextInt() % 30;
                            sheep_pos[0][2] = 0;
                        } else {
                            sheep_pos[i][0] = 0;
                            sheep_pos[i][1] = -1;
                            sheep_pos[i][2] = 0;
                        }
                    }

                    /* run */
                    if(sheep_pos[i][0] > 50 && sheep_pos[i][0] <= 70) {
                        sheep_pos[i][1] -= 3;
                    }

                    /* jump */
                    else {
                        if(sheep_pos[i][0] > 30 && sheep_pos[i][0] <= 50) {
                            sheep_pos[i][1] += 3;
                        }
                        if(sheep_pos[i][0] < 60 && sheep_pos[i][2] == 0) {
                            sheep_number++;
                            sheep_pos[i][2] = 1;
                        }
                    }
                }

            }

        }

    }

    public void paint(Graphics g) {

        if(state == 5) {
            g.setColor(100, 255, 100);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(150, 150, 255);
            g.fillRect(0, 0, getWidth(), getHeight() - 70);

            g.drawImage(background, 35, getHeight() - 80, (g.TOP | g.LEFT));

            for(l = 0; l < sheep_pos.length; l++) {
                if(sheep_pos[l][1] >= 0) {
                    g.drawImage(sheep_image[1],
                            sheep_pos[l][0], sheep_pos[l][1],
                            (g.TOP | g.LEFT));
                }
            }

            g.setColor(0, 0, 0);
            g.drawString(
                    "Number: " + sheep_number,
                    getWidth() - 5,
                    5, (g.TOP | g.RIGHT));

        }

    }

    public void keyPressed(int param) {
        System.out.println("______________ keyPressed " + param + " " + getKeyCode(FIRE));
        System.out.println("______________ keyPressed time - " + System.currentTimeMillis());
        key_flag |= 1 << getKeyNum(param);
    }

    public void keyReleased(int param) {
        System.out.println("______________ keyReleased " + param + " " + getKeyCode(FIRE));
        System.out.println("______________ keyReleased time - " + System.currentTimeMillis());
        key_flag &= ~(1 << getKeyNum(param));
    }

    private boolean isKeyPressed(int key) {
        return (key_flag & 1 << key) != 0;
    }

    private int getKeyNum(int keyin) {
        if(keyin == KEY_NUM0) {
            return KEY_0;
        } else if(keyin == KEY_NUM1) {
            return KEY_1;
        } else if(keyin == KEY_NUM2) {
            return KEY_2;
        } else if(keyin == KEY_NUM3) {
            return KEY_3;
        } else if(keyin == KEY_NUM4) {
            return KEY_4;
        } else if(keyin == KEY_NUM5) {
            return KEY_5;
        } else if(keyin == KEY_NUM6) {
            return KEY_6;
        } else if(keyin == KEY_NUM7) {
            return KEY_7;
        } else if(keyin == KEY_NUM8) {
            return KEY_8;
        } else if(keyin == KEY_NUM9) {
            return KEY_9;
        } else if(keyin == KEY_STAR) {
            return KEY_00;
        } else if(keyin == KEY_POUND) {
            return KEY_01;
        } else if(keyin == KEY_SOFT1) {
            return KEY_SOFT1;
        } else if(keyin == KEY_SOFT2) {
            return KEY_SOFT2;
        } else if(keyin < 0) {
            if(keyin == getKeyCode(UP)) {
                return KEY_UP;
            } else if(keyin == getKeyCode(RIGHT)) {
                return KEY_RIGHT;
            } else if(keyin == getKeyCode(DOWN)) {
                return KEY_DOWN;
            } else if(keyin == getKeyCode(LEFT)) {
                return KEY_LEFT;
            } else if(keyin == getKeyCode(FIRE)) {
                return KEY_SELECT;
            }
        }

        return 31;

    }

    public void writeInt(String file, int param) {

        byte B[] = new byte[4];

        B[3] = (byte)(param & 0x000000ff);
        B[2] = (byte)((param >> 8) & 0x000000ff);
        B[1] = (byte)((param >> 16) & 0x000000ff);
        B[0] = (byte)((param >> 24) & 0x000000ff);

        try {
            RecordStore rs = RecordStore.openRecordStore(file, true);
            RecordEnumeration re = rs.enumerateRecords(null, null, true);

            if (re.hasNextElement()) {
                rs.setRecord(re.nextRecordId(), B, 0, B.length);
            }

            else {
                rs.addRecord(B, 0, B.length);
            }

            rs.closeRecordStore();

        }catch(Exception e) {}

    }

    public int readInt(String file) {

        int param = 0;
        byte[] b = new byte[4];

        try {
            RecordStore rs = RecordStore.openRecordStore(file, true);
            RecordEnumeration re = rs.enumerateRecords(null, null, true);

            if (re.hasNextElement()) {
                b = rs.getRecord(re.nextRecordId());

                param |= (int)((b[0] << 24) & 0xff000000);
                param |= (int)((b[1] << 16) & 0x00ff0000);
                param |= (int)((b[2] << 8) & 0x0000ff00);
                param |= (int)(b[3] & 0x000000ff);
            }

            rs.closeRecordStore();

        }catch(Exception e) {}

        return param;

    }

    public void commandAction(Command command, Displayable display) {

        if(command.equals(cmdZZ)) {
            // writeInt("sheep_number", sheep_number);

            waitDestroy();
            midlet.notifyDestroyed();
        }

    }

    public void waitDestroy() {

        state = -1;

        while(runner.isAlive()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {}
        }

    }

}


